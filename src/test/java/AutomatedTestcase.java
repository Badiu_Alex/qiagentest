import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.LocalDate;

public class AutomatedTestcase {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Alex\\Desktop\\JavaAdvanced\\SeleniumMaven\\drivers\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        driver.get("https://www.hotwire.com/");

        WebElement bundlesButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-bdd='farefinder-option-bundles']")));
        bundlesButton.click();

        WebElement flightButtonFromBundles = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-bdd='farefinder-package-bundleoption-flight']")));
        if(!flightButtonFromBundles.isEnabled()){
            flightButtonFromBundles.click();
        }

        WebElement hotelButtonFromBundles = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-bdd='farefinder-package-bundleoption-hotel']")));
        if(!hotelButtonFromBundles.isEnabled()){
            hotelButtonFromBundles.click();
        }

        WebElement carButtonFromBundles = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-bdd='farefinder-package-bundleoption-car']")));
        if(carButtonFromBundles.isEnabled()){
            carButtonFromBundles.click();
        }

        WebElement fromLocationInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-bdd='farefinder-package-origin-location-input']")));
        fromLocationInput.sendKeys("sfo");
        Thread.sleep(1000);
        fromLocationInput.sendKeys(Keys.RETURN);

        WebElement toLocationInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-bdd='farefinder-package-destination-location-input']")));
        toLocationInput.sendKeys("lax");
        Thread.sleep(1000);
        toLocationInput.sendKeys(Keys.RETURN);

        WebElement departureDateSelect = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-bdd='farefinder-package-startdate-input']")));
        departureDateSelect.click();

        String currentDate = LocalDate.now().getMonth().toString();
        int length = currentDate.length();
        currentDate = currentDate.substring(1,length).toLowerCase();
        System.out.println(currentDate);

        currentDate = "February 29, 2024";

        WebElement specificDate = driver.findElement(By.cssSelector("[aria-label=`${currentDate}`]"));
        specificDate.click();
    }
}
