package org.example;

import java.util.Arrays;

public class AnagramsProblem {

    public static void main(String[] args) {

        String string1 = "Desperation";
        String string2 = "A Rope Ends It";

        string1 = string1.toLowerCase().replaceAll(" ", "");
        string2 = string2.toLowerCase().replaceAll(" ", "");

        if (string1.length() == string2.length()) {

            char[] str1characters = string1.toCharArray();
            char[] str2characters = string2.toCharArray();

            Arrays.sort(str1characters);
            Arrays.sort(str2characters);

            boolean result = Arrays.equals(str1characters, str2characters);

            if (result) {
                System.out.println("The strings provided are anagrams");
            } else {
                System.out.println("The strings provided are NOT anagrams");
            }
        } else {
            System.out.println("The strings provided are NOT anagrams because they don't have the same number of characters");
        }
    }
}
