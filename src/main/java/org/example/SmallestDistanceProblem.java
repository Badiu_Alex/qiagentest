package org.example;

public class SmallestDistanceProblem {

    public static void main(String[] args) {

        int[] ints = new int[]{-1, -2, 6, 1, 3, 9, 4};
        int minDistance = Integer.MAX_VALUE;
        int positiveValue = 0;
        int indexOfNumber = 0;

        for (int i = 0; i < ints.length - 1; i++) {
            if (ints[i] - ints[i + 1] < 0) {
                positiveValue = -(ints[i] - ints[i + 1]);
            } else
                positiveValue = (ints[i] - ints[i + 1]);
            if (positiveValue < minDistance) {
                minDistance = positiveValue;
                indexOfNumber = i;
            }
        }
        System.out.println(indexOfNumber);
        System.out.println(minDistance);
    }
}
